import {onMounted, onUnmounted, ref} from "vue"
function useKeyPress(key:string) {
    let hPress = ref(false)

    function onKeydown() {
        return function (event:KeyboardEvent ) {
            // 获取按下的键
            const keyPressed = event.key;

            // 判断是否按下的是 "h" 键
            if (keyPressed === key) {
                console.log('键盘 "h" 键被按下啦！');
                // 在这里可以执行你想要的操作
                hPress.value = true
            }
        };
    }

    onMounted(()=>{
        document.addEventListener('keydown', onKeydown());
    })
    onUnmounted(()=>{
        document.removeEventListener("mousemove",onKeydown)
    })
    return hPress
}
export  default useKeyPress
