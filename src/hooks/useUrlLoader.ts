import axios from "axios";
import {ref} from "vue";

function useURLloader(url:string ) {
    console.log("useURLloader in");
    const result = ref(null);
    const loading =  ref(true)
    const loaded  = ref(false)
    const  error = ref(null)
    axios.get(url )
        .then(function (response) {
            loading.value  = false
            loaded.value = true
            result.value = response.data
        })
        .catch(function (error) {
            error.value = error
            loading.value = false
        })

    return {
        result,
        loading,
        loaded,
        error,
    }
}

export default useURLloader
