import request from "../utils/request";


export const doLogin = (user:any) => {
    return request({
        url:"/api/login",
        method:"GET",
        data:user
    })
}

export const doLoginTs = (user: API.User): Promise<API.Response<API.LoginResponse>> => {
    return request.post<API.User, API.Response<API.LoginResponse>, API.User>("/login", user);
};
