declare namespace API {
    interface Response<T> {
        data: T;
    }

    interface LoginResponse {
        msg: string;
        result:{
            token: string;
        }
    }

    interface User {
        name: string;
    }
}
