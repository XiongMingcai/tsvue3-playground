import axios from "axios";
let request = axios.create({
    timeout:5000
})
request.interceptors.request.use((config) =>{
    config.url = '/api'+config.url
    return config
})
request.interceptors.response.use((res) =>{
    return res.data
})

export default request
